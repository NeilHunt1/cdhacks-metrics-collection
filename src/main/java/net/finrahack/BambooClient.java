package net.finrahack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.JiraRestClientFactory;
import com.atlassian.jira.rest.client.domain.BasicIssue;
import com.atlassian.jira.rest.client.domain.BasicIssueType;
import com.atlassian.jira.rest.client.domain.BasicProject;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.domain.Project;
import com.atlassian.jira.rest.client.domain.Transition.Field;
import com.atlassian.jira.rest.client.domain.User;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.naming.directory.SearchResult;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;

public class BambooClient {

	// http://localhost:8080/RESTfulExample/json/product/get

	
	private static final String PROD_ISSUES = "# Prod Defects";
	private static final String NEW_FEATURE = "New Feature";
	private static final String JIRA_URL = "http://computech.jira.com";
    private static final String JIRA_ADMIN_USERNAME = "grao";
    private static final String JIRA_ADMIN_PASSWORD = "Lakshmi01";
    private static final String JIRA_ISSUE = "https://computech.jira.com/rest/api/latest/issue/";
	
	//https://computech.jira.com/builds/rest/api/latest/result/BOWL-CDP/latest?expand=jiraIssues
	public static void main(String[] args) throws Exception {

	  try {
		  ArrayList<JiraDtl> jdList = new ArrayList<JiraDtl>();
		  BambooClient bc = new BambooClient();
		  bc.authenticate();
		//URL url = new URL("https://computech.jira.com/builds/rest/api/latest/result/BOWL-CDP/latest.json?expand=jiraIssues");
		
		String urlPart1 = "https://computech.jira.com/builds/rest/api/latest/result/";
		String urlPart2 = ".json?expand=jiraIssues";
		
		for (int i=0;i<4;i++){
		
		Integer y = i+ 5;
		String x = "BOWL-CDP-" + y.toString();
		
		URL url = new URL(urlPart1  + x + urlPart2);
		
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}

		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));
		
		String jsonText = bc.readAll(br);
		String output = br.toString();

		//String output;
		/*System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}*/

		  bc.BambooJSONParser(jsonText, x, jdList);
		
		conn.disconnect();
		}
		
		System.out.println("Build Name  ,  Deployment Date, Number of New features, Number of Production Issues ");
		
		for (JiraDtl jd : jdList){
			System.out.println(jd.getBuildName() + " , " + jd.getDateCreated() + " , " + jd.getNewFeature() + " , " + jd.getProdIssue());
			
		}
		

	  } catch (MalformedURLException e) {

		e.printStackTrace();

	  } catch (IOException e) {

		e.printStackTrace();

	  }

	}
	
	public void authenticate() throws Exception{
		 // Print usage instructions
        StringBuilder intro = new StringBuilder();
        intro.append("**********************************************************************************************\r\n");
        intro.append("* JIRA Java REST Client ('JRJC') example.                                                    *\r\n");
        intro.append("* NOTE: Start JIRA using the Atlassian Plugin SDK before running this example.               *\r\n");
        intro.append("* (for example, use 'atlas-run-standalone --product jira --version 6.0 --data-version 6.0'.) *\r\n");
        intro.append("**********************************************************************************************\r\n");
        System.out.println(intro.toString());

        // Construct the JRJC client
        System.out.println(String.format("Logging in to %s with username '%s' and password '%s'", JIRA_URL, JIRA_ADMIN_USERNAME, JIRA_ADMIN_PASSWORD));
        JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
        URI uri = new URI(JIRA_URL);
        JiraRestClient client = factory.createWithBasicHttpAuthentication(uri, JIRA_ADMIN_USERNAME, JIRA_ADMIN_PASSWORD);

        // Invoke the JRJC Client
        Promise<User> promise = client.getUserClient().getUser("grao");
        User user = promise.claim();
        
        Project prj = client.getProjectClient().getProject("CAPAWS").get();
        
        System.out.println( "Project Lead and Name = " + prj.getLead() +"  " + prj.getName());
        
        // convert object to JSON
        String leadObj = objToJson(prj.getLead());
        
        JSONParser(leadObj);
        
        // Print the result
        System.out.println(String.format("Your admin user's email address is: %s\r\n", user.getEmailAddress()));

        // Done
        System.out.println("Example complete. Now exiting.");
        //System.exit(0);
	}
	
	public void JSONParser(String obj){
		JSONParser parser = new JSONParser();

		try {

			//Object obj = parser.parse(new FileReader("c:\\test.json"));

			JSONParser jp = new JSONParser();
			JSONObject jsonObject =(JSONObject) parser.parse(obj);
			

			String name = (String) jsonObject.get("name");
			System.out.println(name);

			String age = (String)jsonObject.get("self");
			System.out.println(age);

			// loop array
			String msg = (String) jsonObject.get("displayName");
			System.out.println(msg);
			//JSONArray msg = (JSONArray) jsonObject.get("displayName");
			
			/*Iterator<String> iterator = msg.iterator();
			while (iterator.hasNext()) {
				System.out.println(iterator.next());
			}*/

	
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public void  BambooJSONParser(String obj, String releaseName, ArrayList<JiraDtl> jdList){
		JSONParser parser = new JSONParser();
		
		try {

			//Object obj = parser.parse(new FileReader("c:\\test.json"));

			JSONParser jp = new JSONParser();
			JSONObject jsonObject =(JSONObject) parser.parse(obj);
			

			Long name = (Long) jsonObject.get("buildDurationInSeconds");
			System.out.println(name);

			Long age = (Long)jsonObject.get("successfulTestCount");
			System.out.println(age);

			// loop array
			String msg = jsonObject.get("jiraIssues").toString();
			System.out.println(msg);
			
			JSONObject issueList = (JSONObject)parser.parse(msg);
			JSONArray issues = (JSONArray) issueList.get("issue");
			
			   JiraDtl jd = new JiraDtl("", "", 0, 0);
			     
			     
			
			Iterator<JSONObject> iterator = issues.listIterator();
			while (iterator.hasNext()) {
				JSONObject tempObj =iterator.next();
				//JSONObject tempObj = (JSONObject) parser.parse(temp);
				System.out.println(tempObj.get("key"));
				// make the next call and get issuetype
				getIssueDetails( tempObj.get("key").toString(), jd);
				
			}

	
		} catch (ParseException e) {
			e.printStackTrace();
		} catch(Exception ex){
			ex.printStackTrace();
		}
		
		
	}
	
	public String objToJson(Object obj){
		
		Gson gson = new Gson();

		// convert java object to JSON format,
		// and returned as JSON formatted string
		String json = gson.toJson(obj);
		
		
		
	
		System.out.println(json);
		return json;
	}
	
	public String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }
	
	public void getIssueDetails( String issueNumber, JiraDtl jd) throws Exception{
		 // Print usage instructions
       StringBuilder intro = new StringBuilder();
       intro.append("**********************************************************************************************\r\n");
       intro.append("* JIRA Java REST Client ('JRJC') example.                                                    *\r\n");
       intro.append("* NOTE: Start JIRA using the Atlassian Plugin SDK before running this example.               *\r\n");
       intro.append("* (for example, use 'atlas-run-standalone --product jira --version 6.0 --data-version 6.0'.) *\r\n");
       intro.append("**********************************************************************************************\r\n");
       System.out.println(intro.toString());

       // Construct the JRJC client
       System.out.println(String.format("Logging in to %s with username '%s' and password '%s'", JIRA_URL, JIRA_ADMIN_USERNAME, JIRA_ADMIN_PASSWORD));
       JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
       URI uri = new URI(JIRA_URL);
       JiraRestClient client = factory.createWithBasicHttpAuthentication(uri, JIRA_ADMIN_USERNAME, JIRA_ADMIN_PASSWORD);

       
    // Construct the JRJC client
       System.out.println(String.format("Logging in to %s with username '%s' and password '%s'", JIRA_URL, JIRA_ADMIN_USERNAME, JIRA_ADMIN_PASSWORD));
       //JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
       //URI uri = new URI(JIRA_URL);
       //JiraRestClient client = factory.createWithBasicHttpAuthentication(uri, JIRA_ADMIN_USERNAME, JIRA_ADMIN_PASSWORD);

       // Invoke the JRJC Client
       Promise<User> promise = client.getUserClient().getUser("grao");
       User user = promise.claim();

      /* for (BasicProject project : client.getProjectClient().getAllProjects().claim()) {
           System.out.println(project.getKey() + ": " + project.getName());
       }*/

       Promise<Issue> issuedtl = client.getIssueClient().getIssue(issueNumber);
      // Promise<SearchResult> searchJqlPromise = client.getSearchClient().searchJql(issueNumber);
       //Promise<SearchResult> searchJqlPromise = client.getSearchClient().searchJql("project = BOWL and key= '" + issueNumber + "'");

     Iterable<com.atlassian.jira.rest.client.domain.Field> fieldItr =  issuedtl.get().getFields();
     
     String creationDate = issuedtl.get().getCreationDate().toString();
     jd.setDateCreated(creationDate);
     
     BasicIssueType bt = issuedtl.get().getIssueType();
     System.out.println(bt.getName());
     if (bt.getName().equals(PROD_ISSUES)){
    	 jd.setProdIssue(jd.getProdIssue() + 1);
     } else if (bt.getName().equals(NEW_FEATURE)){
    	 jd.setNewFeature(jd.getNewFeature() + 1);
     }
       
  
     
       //System.out.println( issuedtl.get().getFields())   getFieldByName("issueType"));
       
       
       // Print the result
       System.out.println(String.format("Your admin user's email address is: %s\r\n", user.getEmailAddress()));

       // Done
       System.out.println("Example complete. Now exiting.");
       //System.exit(0);
       
       
       
       
       
       
       // Invoke the JRJC Client
       //Promise<User> promise = client.getUserClient().getUser("grao");
       //User user = promise.claim();
       
       Project prj = client.getProjectClient().getProject("BOWL").get();
       
       System.out.println( "Project Lead and Name = " + prj.getLead() +"  " + prj.getName());
       
       // convert object to JSON
       String leadObj = objToJson(prj.getLead());
       
       JSONParser(leadObj);
       
       // Print the result
       System.out.println(String.format("Your admin user's email address is: %s\r\n", user.getEmailAddress()));

       // Done
       System.out.println("Example complete. Now exiting.");
       //System.exit(0);
	}

}
