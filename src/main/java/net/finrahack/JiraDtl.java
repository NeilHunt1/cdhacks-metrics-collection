package net.finrahack;

import java.io.Serializable;

public class JiraDtl implements Serializable {

	private String buildName;
	private Integer prodIssue;
	private Integer newFeature;
	private String dateCreated;
	
	

	
	public String getBuildName() {
		return buildName;
	}




	public void setBuildName(String buildName) {
		this.buildName = buildName;
	}




	public Integer getProdIssue() {
		return prodIssue;
	}




	public void setProdIssue(Integer prodIssue) {
		this.prodIssue = prodIssue;
	}




	public Integer getNewFeature() {
		return newFeature;
	}




	public void setNewFeature(Integer newFeature) {
		this.newFeature = newFeature;
	}




	public String getDateCreated() {
		return dateCreated;
	}




	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}


	public JiraDtl(String x, String y, int z, int a) {
		// TODO Auto-generated constructor stub
		
		this.setBuildName("");
		this.setDateCreated("");
		this.setNewFeature(0);
		this.setProdIssue(0);
	}

	public JiraDtl() {
		// TODO Auto-generated constructor stub
	}



}
